
const { generarArchivo } = require('./helpers/multiplicar');
const argv = require('yargs').argv;
//Estoy importando el paquete colors
require('colors');

generarArchivo(argv.base)
 .then(nombreArchivo => console.log(nombreArchivo.rainbow, 'creado'))
 .catch(err => console.log(err));cd